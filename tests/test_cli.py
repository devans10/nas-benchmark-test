import pytest

from nasbenchmark import cli

datadir = "/path/to/data"
workspace = "/workspace"
archive = "/archive"

@pytest.fixture
def parser():
    return cli.create_parser()

def test_parser_without_data(parser):
    """
    Without a specified data directory the parser will exit
    """
    with pytest.raises(SystemExit):
        parser.parse_args(["--workspace", workspace, "--archive", archive])

def test_parser_without_workspace(parser):
    """
    Without a specified workspace directory the parser will exit
    """
    with pytest.raises(SystemExit):
        parser.parse_args(["--data", datadir, "--archive", archive])

def test_parser_without_archive(parser):
    """
    Without a specified archive directory the parser will exit
    """
    with pytest.raises(SystemExit):
        parser.parse_args(["--data", datadir, "--workspace", workspace])

def test_parser_with_required_args(parser):
    """
    The parser will not exit if it receives all required args
    """
    args = parser.parse_args(["--data", datadir, "--workspace", workspace, "--archive", archive])

    assert args.data == datadir
    assert args.workspace == workspace
    assert args.archive == archive

def test_parser_set_buffer(parser):
    """
    Test the buffer gets optionally set
    """
    args = parser.parse_args(["--data", datadir, "--workspace", workspace, "--archive", archive, "--buffer", "20"])
    assert args.buffer == "20"
