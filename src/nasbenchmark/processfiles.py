#!/usr/bin/python

import os
import sys
import time
import glob
from threading import Thread
from queue import Queue

def processfile(item):
    dest_file = '/data_arch/'+item.split('/')[2]
    with open(item, 'r') as src:
        with open(dest_file, 'w') as dest:
            dest.write(src.read())
            os.fsync(dest)
    
def worker():
    while True:
      item = q.get()
      processfile(item)
      q.task_done()

q = Queue()
for i in range(7):
    t = Thread(target=worker)
    t.daemon = True
    t.start()

files = glob.glob('/data/*')
for infile in files:
    q.put(infile)

q.join()
