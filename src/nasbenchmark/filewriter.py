#!/usr/bin/python

import os
import sys
import time
import glob
import concurrent.futures

source_dir = "/data"
dest_dir = "/data_write"
arch_dir= "/data_arch"


count = 0
filecount = 0
byteswritten = 0
runtime = 0
starttime = time.time()
for folder, subs, files in os.walk(source_dir):
    for filename in files:
        with open(os.path.join(folder, filename), 'r') as src:
            with open(dest_dir+'/'+filename, 'w') as dest:
                dest.write(src.read())
                os.fsync(dest)
                byteswritten += os.path.getsize(dest_dir+'/'+filename)
                count += 1
                filecount += 1
                if count == 1000:
                    runtime = runtime + (time.time() - starttime)
                    starttime = time.time()
                    time.sleep(10)
                    count = 0
              
runtime = runtime + (time.time() - starttime)

print("%s seconds" % (runtime))
print("%s files written" % filecount)
print("%s files/sec" % (filecount / runtime))
print("%s MB written" % (byteswritten / (1024*1024)))
print("%s bytes/sec written" % ((byteswritten / (1024*1024)) / runtime))

