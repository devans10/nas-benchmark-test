import pytest
import tempfile

from nasbenchmark import fileprocessor

@pytest.fixture
def infile():
    f = tempfile.TemporaryFile('r+b')
    f.write(b"Testing")
    f.seek(0)
    return f

def test_writing_files(infile):
    """
    Writes content from one file-like to another
    """
    outfile = tempfile.NamedTemporaryFile(delete=False)
    timetaken, io, byteswritten = fileprocessor.writefile(infile, outfile, buffersize=8196)
    with open(outfile.name, 'rb') as f:
        assert f.read() == b"Testing"
    
    assert timetaken != None
    assert io == 1
    assert byteswritten == 7

def test_reading_files(infile):
    """
    Tests that stats are returned from the filewriter.writedata process.
    """
    timetaken, io, bytesread = fileprocessor.readfile(infile, buffersize=8196)
    
    assert timetaken != None
    assert io == 1
    assert bytesread == 7

def test_move_files(infile):
    """
    Tests moving a file
    """
    outfile = tempfile.NamedTemporaryFile(delete=False)
    timetaken, io, byteswritten, bytesread = fileprocessor.movefile(infile, outfile, buffersize=8196)
    with open(outfile.name, 'rb') as f:
        assert f.read() == b"Testing"

    assert timetaken != None
    assert io == 2
    assert byteswritten == 7
    assert bytesread == 7
