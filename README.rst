nasbenchmark
==================

NAS benchmark test 

Preparing for Development
-------------------------

1. Ensure ``pip`` and ``pipenv`` are installed
2. Clone repository: ``git clone git@gitlab.com:devans10/nas-benchmark-test``
3. ``cd`` into repository
4. Fetch development dependencies ``make install``
5. Activate virtualenv: ``pipenv shell``

Usage
-----

Pass in a directory containing the test data, a workspace directory, and an archive directory.

The directory with the test data should be local to the server.
The workspace and archive directories should be NAS mountpoints.

Example:

::

    $ nasbenchmark --data /path/to/test/data  --workspace /workspace  --archive /archive



Running Tests
-------------

Run tests locally using ``make`` if virtualenv is active:

::

    $ make



If virtualenv isn’t active then use:

::

    $ pipenv run make
