import io
from argparse import ArgumentParser

def create_parser():
    parser = ArgumentParser(description="""
    Perform NAS Benchmark Tests.
    """)
    parser.add_argument("--data", required=True, help="Local directory containing test data")
    parser.add_argument("--workspace", required=True, help="NAS Mounted workspace directory")
    parser.add_argument("--archive", required=True, help="NAS Mounted archive directory")
    parser.add_argument("--buffer", default=io.DEFAULT_BUFFER_SIZE, help="I/O buffer size")

    return parser

def buildqueue(q, dirs):    
    count = 0
    for infile in glob.iglob(dirs['datadir']+'/*'):
        q.put(infile.split('/')[-1])
        count += 1
        if count == 1000:
            time.sleep(10)
            count = 0

def worker(dirs, io_v, timetaken_v, byteswritten_v, bytesread_v, buffersize, item):
    datadir = dirs['datadir']
    workspace = dirs['workspace']
    archive = dirs['archive']
      
    #Write file to workspace directory
    infile = open(f"{datadir}/{item}", 'rb', buffersize)
    outfile = open(f"{workspace}/{item}", 'wb', buffersize)
    timetaken, io, byteswritten = fileprocessor.writefile(infile, outfile, buffersize)
    infile.close()
    outfile.close()
    timetaken_v += timetaken
    io_v += io
    byteswritten_v += byteswritten

    #Read file from workspace directory
    infile = open(f"{workspace}/{item}", 'wb', buffersize)
    timetaken, io, byteswritten = fileprocessor.readfile(infile, buffersize)
    infile.close()
    timetaken_v += timetaken
    io_v += io
    bytesread_v += bytesread
      
    #Move file to Archive
    infile = open(f"{workspace}/{item}", 'rb', buffersize)
    outfile = open(f"{archive}/{item}", 'wb', buffersize)
    timetaken, io, byteswritten, bytesread = fileprocessor.movefile(infile, outfile, buffersize)
    infile.close()
    outfile.close()
    timetaken_v += timetaken
    io_v += io
    byteswritten_v += byteswritten
    bytesread_v += bytesread


def main():
    import multiprocessing
    from functools import partial
    from nasbenchmark import fileprocessor
    args = create_parser().parse_args()

    manager = multiprocessing.Manager
    filequeue = manager.Queue()
    io_value = manager.Value(int, 0)
    timetaken_value = manager.Value(float, 0)
    byteswritten_value = manager.Value(int, 0)
    bytesread_value = manager.Value(int, 0)
    data_dict = manager.dict({'datadir': args.data,
                              'workspace': args.workspace, 
                              'archive': args.archive})

    build_queue_p = multiprocessing.Process(target=buildqueue, args=(filequeue, data_dict))
    build_queue_p.start()

    with multiprocessing.Pool(processes=) as pool:
        pool.map(partial(worker, data_dict, io_value, timetaken_value, byteswritten_value, bytesread_value, args.buffersize), (filequeue,))

    print(f"Time Taken: {timetaken_value}")
    print(f"Bytes Written: {byteswritten_value}")
    print(f"Bytes Read: {bytesread_value}")
    print(f"I/O: {io}")
    print(f"IOPS: {io / timetaken_value}")
    print(f"Throughput: {(byteswritten_value + bytesread_value) / timetaken}/s")
