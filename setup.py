from setuptools import setup, find_packages

with open('README.rst', encoding='UTF-8') as f:
    readme = f.read()

setup(
    name='nasbenchmark',
    version='0.1.0',
    description='NAS Benchmark test for DLC Meter Interagation Simulation',
    long_description=readme,
    author='Dave Evans',
    author_email='devans@duqlight.com',
    packages=find_packages('src'),
    package_dir={'': 'src'},
    install_requires=[]
)
