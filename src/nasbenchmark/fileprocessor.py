import time
import math

def writefile(infile, outfile, buffersize):
    starttime = time.time()
    outfile.write(infile.read())
    outfile.flush()
    timetaken = time.time() - starttime
    byteswritten = outfile.tell()
    io = math.ceil(byteswritten / buffersize)

    return timetaken, io, byteswritten

def readfile(infile, buffersize):
    starttime = time.time()
    f = infile.read()
    timetaken = time.time() - starttime
    bytesread = infile.tell()
    io = math.ceil(bytesread / buffersize)

    return timetaken, io, bytesread

def movefile(infile, outfile, buffersize):
    starttime = time.time()
    outfile.write(infile.read())
    outfile.flush()
    timetaken = time.time() - starttime
    byteswritten = outfile.tell()
    bytesread = infile.tell()
    io = math.ceil(bytesread / buffersize) + math.ceil(byteswritten / buffersize)

    return timetaken, io, byteswritten, bytesread

